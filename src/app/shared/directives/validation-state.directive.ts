import { ContentChild, Directive, ElementRef, Renderer } from '@angular/core';
import { FormControlName } from '@angular/forms';

@Directive({
  selector: '[validationState]'
})
export class ValidationStateDirective {
  @ContentChild(FormControlName)
  public formControl: FormControlName;

  constructor(
    private elem: ElementRef,
    private renderer: Renderer
  ) { }

  ngAfterContentInit() {
    const isValid = typeof this.formControl != 'undefined' && this.formControl instanceof FormControlName;
    if (isValid) {
      this.formControl.statusChanges.subscribe(status => this.toggleStateClass(status));
    }
  }

  public toggleStateClass(status: 'VALID' | 'INVALID') {
    const nextState = status === 'VALID' ? 'is-valid' : 'is-invalid';
    const prevState = nextState === 'is-valid' ? 'is-invalid' : 'is-valid';
    const element = this.elem.nativeElement as HTMLInputElement;
    this.renderer.setElementClass(element, nextState, true);
    this.renderer.setElementClass(element, prevState, false);
  }
}
