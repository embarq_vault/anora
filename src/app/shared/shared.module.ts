import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ValidationStateDirective } from './directives/validation-state.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ ValidationStateDirective ],
  exports: [ ValidationStateDirective ]
})
export class SharedModule { }
