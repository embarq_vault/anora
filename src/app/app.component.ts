import { Component } from '@angular/core';
import { Router, NavigationCancel } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
  constructor(router: Router) {
    router.events
      .filter(event => event instanceof NavigationCancel)
      .subscribe(x => router.navigate(['/unauthorized']));
  }
}
