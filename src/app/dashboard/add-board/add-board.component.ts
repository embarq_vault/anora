import {
  Component,
  EventEmitter,
  OnInit,
  Output
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

type ButtonMode = 'button' | 'form';

@Component({
  selector: 'add-board-button',
  templateUrl: './add-board.component.html',
  styles: []
})
export class AddBoardComponent implements OnInit {
  @Output('onAdd')
  public onAdd: EventEmitter<string>;

  public boardForm: FormGroup;
  public mode: ButtonMode;

  constructor() {
    this.onAdd = new EventEmitter<string>(true);
    this.mode = 'button';
  }

  public ngOnInit() {
    this.boardForm = new FormGroup({
      boardName: new FormControl(null, Validators.required)
    });
  }

  public select(mode: ButtonMode) {
    this.mode = mode;
    this.boardForm.reset();
  }

  public handleSubmit() {
    this.onAdd.emit(this.boardForm.value.boardName);
    this.boardForm.reset();
  }

}
