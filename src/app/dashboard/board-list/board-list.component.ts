import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BoardService } from '../../providers/board.service';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.scss']
})
export class BoardListComponent implements OnInit {
  public boards$: Observable<any>;

  constructor(private boards: BoardService) { }

  public ngOnInit() {
    this.boards$ = this.boards.get();
  }

  public handleBoardAdd(boardName: string) {
    this.boards
      .create({ name: boardName, ownerId: null })
      .subscribe();
  }

}
