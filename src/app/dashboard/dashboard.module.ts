import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { DashboardRouterModule } from './dashboard.routing';

import { DashboardComponent } from './dashboard.component';
import { HeadingComponent } from './heading/heading.component';
import { BoardListComponent } from './board-list/board-list.component';
import { BoardComponent } from './board/board.component';
import { BoardGridListComponent } from './board-grid-list/board-grid-list.component';
import { AddBoardComponent } from './add-board/add-board.component';

@NgModule({
  imports: [
    CommonModule,
    DashboardRouterModule,
    ReactiveFormsModule
  ],
  declarations: [
    DashboardComponent,
    HeadingComponent,
    BoardListComponent,
    BoardComponent,
    BoardGridListComponent,
    AddBoardComponent
  ],
  entryComponents: [
    DashboardComponent,
    HeadingComponent,
    BoardListComponent,
    BoardComponent,
    BoardGridListComponent,
    AddBoardComponent
  ]
})
export class DashboardModule { }
