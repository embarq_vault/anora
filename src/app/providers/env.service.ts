import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

export class EnvService {
  public readonly origin: string;

  constructor() {
    this.origin = environment.production ? '/api' : 'https://anora.herokuapp.com/api';
  }

}
