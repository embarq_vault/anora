import { Injectable } from '@angular/core';
import { LocalStorageService } from './local-storage.service';

@Injectable()
export class IdService {
  public set id(value: string | null) {
    if (value == null) {
      this.localStorage.storage.removeItem('uid');
    } else {
      this.localStorage.storage.setItem('uid', value);
    }
  }

  public get id() {
    return this.localStorage.storage.getItem('uid');
  }

  public set userId(value: string | null) {
    if (value == null) {
      this.localStorage.storage.removeItem('id');
    } else {
      this.localStorage.storage.setItem('id', value);
    }
  }

  public get userId() {
    return this.localStorage.storage.getItem('id');
  }

  constructor(private localStorage: LocalStorageService) { }

}
