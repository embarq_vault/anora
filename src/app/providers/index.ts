import { AuthService } from './auth.service';
import { BoardService } from './board.service';
import { EnvService } from './env.service';
import { IdService } from './id.service';
import { LocalStorageService } from './local-storage.service';
import { RestService } from './rest.service';

export const Providers = [
  AuthService,
  BoardService,
  EnvService,
  IdService,
  LocalStorageService,
  RestService
];

export {
  AuthService,
  BoardService,
  EnvService,
  IdService,
  LocalStorageService,
  RestService
}
