import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHandler,
  HttpHeaders,
  HttpParams,
  HttpRequest
} from '@angular/common/http';

import { EnvService } from './env.service';
import { IdService } from './id.service';

export interface RequestOptions {
  body?: any;
  params?: HttpParams;
  headers?: HttpHeaders;
  reportProgress?: boolean;
  withToken?: boolean;
}

@Injectable()
export class RestService {
  constructor(
    private http: HttpClient,
    private env: EnvService,
    private id: IdService
  ) { }

  public get authOptions(): RequestOptions {
    return { withToken: true }
  }

  public call<T>(method: string, urlItem: string, options: RequestOptions) {
    const url = `${ this.env.origin }/${ urlItem }`;
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    if (options.withToken) {
      headers['Authorization'] = this.id.id;
    }

    const config = { ...options, headers: new HttpHeaders(headers) };
    return this.http.request<T>(method, url, config);
  }

  public get<T>(url: string, options: RequestOptions = {}) {
    return this.call<T>('get', url, options);
  }

  public post<T>(url: string, body: any, options: RequestOptions = {}) {
    return this.call<T>('post', url, { ...options, body });
  }

  public put<T>(url: string, body: any, options: RequestOptions = {}) {
    return this.call<T>('put', url, { ...options, body });
  }

  public patch<T>(url: string, body: any, options: RequestOptions = {}) {
    return this.call<T>('patch', url, { ...options, body });
  }

  public delete<T>(url: string, options: RequestOptions = {}) {
    return this.call<T>('delete', url, options);
  }

}
