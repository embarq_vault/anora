export class Store implements Storage {
  [key: string]: any;
  [index: number]: string;

  private store: { [key: string]: string };

  constructor() {
    this.store = {};
  }

  public setItem(key: string, value: string): void {
    this.store[key] = value;
  }

  public getItem(key): string | null {
    return this.store[key] || null;
  }

  public removeItem(key) {
    this.store[key] = void 0;
  }

  public clear() {
    this.store = {};
  }

  public get length(): number {
    return Object.keys(this.store).length;
  }

  public key(index: number) {
    return Object.keys(this.store)[index];
  }
}

export class LocalStorageService {
  public storage: Storage;

  constructor() {
    this.storage = typeof window !== 'undefined' ? localStorage : new Store();
  }
}
