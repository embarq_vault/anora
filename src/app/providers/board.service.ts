import { Injectable } from '@angular/core';

import { RestService } from './rest.service';
import { IdService } from './id.service';
import { Board } from '../models/board';

@Injectable()
export class BoardService {
  constructor(
    private rest: RestService,
    private id: IdService
  ) { }

  public get() {
    return this.rest.get<Board.Entries>('boards', this.rest.authOptions);
  }

  public create(board: Board.Entry) {
    const body = Object.assign({}, board, { ownerId: this.id.userId });
    return this.rest.post<Board.Entry>('boards', body, this.rest.authOptions);
  }
}
