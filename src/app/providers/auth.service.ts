import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { IdService } from './id.service';
import { RestService } from './rest.service';

import { User, Credentials } from '../models/user';

export interface LoginResponse {
  id: string;
  ttl: number;
  created: Date & string;
  userId: string;
}

export interface RegisterResponse {
  name: string;
  email: string;
  id: string;
  username?: string;
}

export enum AuthState {
  Unauthorized = 0,
  Authorized
}

@Injectable()
export class AuthService {
  public authState$: BehaviorSubject<AuthState>;

  constructor(
    private http: HttpClient,
    private id: IdService,
    private rest: RestService
  ) {
    const state = this.id.id == null ? AuthState.Unauthorized : AuthState.Authorized;
    this.authState$ = new BehaviorSubject<AuthState>(state);
  }

  public login(credentials: Credentials) {
    return this.rest
      .post<LoginResponse>('users/login', credentials)
      .do(res => {
        this.id.id = res.id;
        this.id.userId = res.userId;
        this.authState$.next(AuthState.Authorized);
      });
  }

  public register(user: User) {
    return this.rest.post<RegisterResponse>('users', user)
  }

  public logout() {
    return this.rest
      .post(`users/logout?accessToken=${ this.id.id }`, null)
      .do(() => {
        this.id.id = null;
        this.id.userId = null;
        this.authState$.next(AuthState.Unauthorized);
      });
  }

}
