import { Injectable } from '@angular/core';
import { Route, CanLoad } from '@angular/router';
import { AuthService, AuthState } from './auth.service';

@Injectable()
export class AuthGuardService implements CanLoad {
  constructor(private auth: AuthService) { }

  public canLoad(route: Route) {
    return this.auth.authState$.getValue() === AuthState.Authorized;
  }
}
