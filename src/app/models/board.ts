import { Card } from './card';
import { List } from './list';
import { User } from './user';

export namespace Board {
  export interface Entry {
    id?: string;
    name: string;
    ownerId: string;
    lists?: Card.Entries;
    cards?: List.Entries;
    owner?: User;
  }

  export type Entries = Array<Entry>;
}
