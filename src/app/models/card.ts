export namespace Card {
  export interface Entry {
    name: string;
    description: string;
    id?: string;
    listId?: string;
    boardId?: string;
  }

  export type Entries = Array<Entry>;
}
