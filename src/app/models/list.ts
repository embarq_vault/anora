export namespace List {
  export interface Entry {
    name: string;
    id?: string;
    boardId?: string;
  }

  export type Entries = Array<Entry>;
}
