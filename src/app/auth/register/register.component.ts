import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../providers/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public registerForm: FormGroup;
  public validationErrors;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.validationErrors = [
      {
        type: 'required',
        label: 'This field is required',
      },
      {
        type: 'email',
        label: 'Invalid email',
      },
      {
        type: 'minlength',
        label: 'Must be at least 8 characters'
      },
    ];
  }

  public ngOnInit() {
    this.registerForm = new FormGroup({
      name: new FormControl(null, [ Validators.required ]),
      username: new FormControl(null),
      email: new FormControl(null, [ Validators.required, Validators.email ]),
      password: new FormControl(null, [ Validators.required, Validators.minLength(8) ]),
    });
  }

  public register() {
    this.auth
      .register(this.registerForm.value)
      .subscribe(
        res => {
          this.toastr.success('Successfully registered!');
          this.router.navigate(['/auth', 'login']);
        },
        res => {
          this.toastr.error('An error occured');
        });
  }

}
