import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public validationErrors;

  constructor(
    private auth: AuthService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.validationErrors = [
      {
        type: 'required',
        label: 'This field is required',
      },
      {
        type: 'email',
        label: 'Invalid email',
      },
      {
        type: 'minlength',
        label: 'Must be at least 8 characters'
      },
    ];
  }

  public ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [ Validators.required, Validators.email ]),
      password: new FormControl(null, [ Validators.required, Validators.minLength(8) ]),
    });
  }

  public login() {
    this.auth
      .login(this.loginForm.value)
      .subscribe(
        res => {
          this.toastr.success('Successfully logged in!');
          this.router.navigate(['/dashboard']);
        },
        res => {
          this.toastr.error('An error occured');
        });
  }

}
